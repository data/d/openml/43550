# OpenML dataset: COVID-19-Hospitals-Treatment-Plan

https://www.openml.org/d/43550

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The COVID-19 pandemic has placed an unprecedented strain on health systems, with rapidly increasing demand for healthcare in hospitals and intensive care units (ICUs) worldwide. As the pandemic escalates, determining the resulting needs for healthcare resources (beds, staff, equipment) has become a key priority for many countries. Projecting future demand requires estimates of how long patients with COVID-19 need different levels of hospital care.
 While healthcare management has various use cases for using data science, patient length of stay is one critical parameter to observe and predict if one wants to improve the efficiency of the healthcare management in a hospital. 
This parameter helps hospitals to identify patients of high LOS risk (patients who will stay longer) at the time of admission. Once identified, patients with high LOS risk can have their treatment plan optimized to miminize LOS and lower the chance of staff/visitor infection. Also, prior knowledge of LOS can aid in logistics such as room and bed allocation planning.
The problem is to manage the functioning of Hospitals in a professional and optimal manner. 
 
 
 

Content
The task is to accurately predict the Length of Stay for each patient on case by case basis so that the Hospitals can use this information for optimal resource allocation and better functioning. The length of stay is divided into 11 different classes ranging from 0-10 days to more than 100 days.
Data : host_train.csv  File containing features related to patient, hospital and Length of stay on case basis
For each record in the dataset the following is provided::

  
  case_id                           
  Hospital                          
  Hospital_type                
  Hospital_city                  
  Hospital_region              
  Available-Extra-Rooms-in-Hospital        Number of Extra rooms available in the Hospital
  Department                      Department overlooking the case  ['radiotherapy' 'anesthesia' 'gynecology' 'TB  Chest disease' 'surgery']
  Ward_Type                       ['R' 'S' 'Q' 'P' 'T' 'U']
  Ward_Facility                   ['F' 'E' 'D' 'B' 'A' 'C']
  Bed_Grade                        Condition of Bed in the Ward
  patientid                        
  CityCodePatient           City Code for the patient
  Type of Admission            Admission Type registered by the Hospital   ['Emergency' 'Trauma' 'Urgent']
  Illness_Severity               Severity of the illness recorded at the time of admission   ['Extreme' 'Moderate' 'Minor']
  Patient_Visitors            
  Age                             Age category   ['51-60' '71-80' '31-40' '41-50' '81-90' '61-70' '21-30' '11-20' '0-10' '91-100']
  Admission_Deposit            Deposit at the Admission Time
  Stay_Days                           Stay Days by the patient (target)  ['0-10' '41-50' '31-40' '11-20' '51-60' '21-30' '71-80'
  'More than 100 Days' '81-90' '61-70' '91-100']
  

Starter Kernels

EDA and Random Forest Benchmark

Inspiration

Predict the Length of Stay for each patient
Interpret best model(s) and  mine influence factors in LOS risk 

More References

COVID-19 length of hospital stay
A retrospective cohort study in a Fangcang shelter hospital

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43550) of an [OpenML dataset](https://www.openml.org/d/43550). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43550/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43550/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43550/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

